package com.luisignacio.com.nearadmin.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class TablesListAdapter extends RecyclerView.Adapter<TablesListAdapter.MyViewHolder>{

    private Context ctx;
    private HashMap<String, Table> mTables;
    private View mParent;
    private Interfaces.RecyclerViewCLickListener mListener;
    private ArrayList<Table> mTablesList;

    public TablesListAdapter(Context ctx, HashMap<String, Table> mTables, View mParent, Interfaces.RecyclerViewCLickListener mListener) {
        this.ctx = ctx;
        this.mTables = mTables;
        this.mParent = mParent;
        this.mListener = mListener;
        init();
    }

    public Context getCtx() {
        return ctx;
    }

    public void setTablesEstate(HashMap<String, Table> tables ){
        mTables = tables;
        init();
        notifyDataSetChanged();
    }

    private void init() {
        mTablesList = new ArrayList<>();

        for ( Table item : mTables.values() )
            mTablesList.add(item);

        Collections.sort(mTablesList, new Comparator<Table>() {
            @Override
            public int compare(Table table1, Table table2) {
                if ( table1.getId() == null || table2.getId() == null)
                    return 0;
                else{
                    return Integer.parseInt(table1.getId()) - Integer.parseInt(table2.getId());
                }
            }
        });

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(ctx).inflate(R.layout.table_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.binItem(mTablesList.get(position), ctx);
    }

    @Override
    public int getItemCount() {
        return mTablesList.size();
    }

    public Table getItem(int index){
        return ((mTablesList != null && mTablesList.size() > index)?mTablesList.get(index):null);
    }

    protected static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mTableId, mTvOccupied, mTvOccupiedContent;
        private Button mBtnAction;
        private Interfaces.RecyclerViewCLickListener mListener;
        private CardView mCard_table;
        public MyViewHolder(View itemView, Interfaces.RecyclerViewCLickListener listener) {
            super(itemView);

            mListener = listener;

            mCard_table = itemView.findViewById(R.id.card_table);
            mTableId = itemView.findViewById(R.id.table_id);
            mTvOccupied = itemView.findViewById(R.id.tvOccupied);
            mTvOccupiedContent = itemView.findViewById(R.id.tvOccupiedContent);
            mBtnAction = itemView.findViewById(R.id.btnAction);
            mBtnAction.setOnClickListener(this);

        }

        public void binItem( Table table, Context ctx ){
            mTableId.setText(table.getId());
            mTableId.setTextColor(ctx.getResources().getColor((table.getState() == 0) ? R.color.white : R.color.black));
            mBtnAction.setText((table.getUser_id() == null) ? ctx.getString(R.string.occupy) : ctx.getString(R.string.free));
            mTableId.getBackground().setLevel(table.getState());
            mTvOccupied.setText((table.getUser_id() == null) ? ctx.getString(R.string.free_table) : ctx.getString(R.string.occupied_since));
            mTvOccupiedContent.setVisibility((table.getUser_id() == null) ? View.GONE : View.VISIBLE);
            if (table.getUser_id() != null){
                mTvOccupiedContent.setText(Utils.getStringTime(table.getStart_time()));
            }
            mCard_table.setCardBackgroundColor(ctx.getResources().getColor((table.getUser_id() == null) ? R.color.free : R.color.occupied));
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }

    }

}
