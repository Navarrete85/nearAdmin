package com.luisignacio.com.nearadmin.Utils;

import android.os.Handler;
import android.util.Log;
import android.widget.TextView;


public class CustomTimer implements Runnable {


    public long millisUntilFinished;
    public TextView holder;
    Handler handler;

    public CustomTimer (Handler handler,TextView holder,long millisUntilFinished) {
        this.handler = handler;
        this.holder = holder;
        this.millisUntilFinished = millisUntilFinished;

    }

    @Override
    public void run() {
        /* do what you need to do */
        long seconds = millisUntilFinished / 1000;
        long minutes = seconds / 60;

        String time;
        if ( minutes == 0 )
            time = seconds % 60 + ((seconds == 1) ? " segundo" : " segundos");
        else
            time = minutes % 60 + ((minutes == 1) ? " minuto" : " minutos");

        holder.setText(time);



        Log.d("DEV123",time);
        /* and here comes the "trick" */
        if (minutes == 0) {
            millisUntilFinished += 1000;
            handler.postDelayed(this, 1000);
        }else {
            millisUntilFinished += 10000;
            handler.postDelayed(this, 10000);
        }
    }

}
