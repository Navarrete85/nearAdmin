package com.luisignacio.com.nearadmin.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luisignacio.com.nearadmin.Activitys.MainActivity;
import com.luisignacio.com.nearadmin.Adapter.SwipeController;
import com.luisignacio.com.nearadmin.Adapter.SwipeControllerActions;
import com.luisignacio.com.nearadmin.Adapter.TablesListAdapter;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.Models.Tables;
import com.luisignacio.com.nearadmin.Models.User;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Utils.Utils;

import java.util.HashMap;

import static com.luisignacio.com.nearadmin.Utils.Constants.TABLES;
import static com.luisignacio.com.nearadmin.Utils.Constants.USERS;

/**
 */
public class TablesList extends Fragment {

    private static final String TAG = "TablesList";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Tables mTables;
    private View mView;
    private RecyclerView mRecyclerView;
    private TablesListAdapter mAdapter;
    private Interfaces.OnLoaderAction mListenerLoader;

    public void setmTables(Tables mTables) {
        this.mTables = mTables;
        if ( mAdapter != null )
            mAdapter.setTablesEstate(mTables.getTables());
    }

    private String mParam2;

    private Interfaces.OnFragmentInteractionListener mListener;

    public TablesList() {}

    public static TablesList newInstance(Tables tables, String param2) {
        TablesList fragment = new TablesList();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, tables);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTables = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_tables_list, container, false);
        mRecyclerView = mView.findViewById(R.id.table_list);
        Interfaces.RecyclerViewCLickListener listener = (view, position) -> {

            final Table item = mAdapter.getItem(position);
            showMessage(item);

        };

        mAdapter = new TablesListAdapter(getContext(), (mTables == null || mTables.getTables() == null)?new HashMap<String, Table>():mTables.getTables(), mView, listener);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        return mView;
    }

    private void showMessage(Table item) {

        View dialog = getDialog();
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setView(dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.findViewById(R.id.btn_dialog).setOnClickListener((view) -> {
            mListenerLoader.onStarLoader(view);
            final String establishment_id = ((MainActivity)getActivity()).getmEstablishment_id();
            if ( item.getUser_id() == null ){
                item.setUser_id("DefaultUser");
                item.setTime_of_state_change(null);
                item.setStart_time(Utils.getStringCurrentTime());
                item.setState(0);
            }else{
                clearUser(item.getUser_id());
                item.setStart_time(null);
                item.setState(0);
                item.setUser_id(null);
                item.setTime_of_state_change(null);
            }
            FirebaseDatabase.getInstance().getReference().getRoot().child(TABLES).child(establishment_id).child(item.getTable_id()).setValue(item).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.i(TAG, (task.isComplete() ? "Completado" : "No completado"));
                    Log.i(TAG, task.isSuccessful() + " Exito?");
                    mListenerLoader.onStopLoader(view);
                }
            });
            alertDialog.dismiss();
        });
        final Button cancel = dialog.findViewById(R.id.btn_dialog_cancel);
        cancel.setVisibility(View.VISIBLE);
        cancel.setOnClickListener((view) -> alertDialog.dismiss());
        alertDialog.show();

    }

    private void clearUser(String user_id) {
        FirebaseDatabase.getInstance().getReference().getRoot().child(USERS).child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if ( dataSnapshot.exists() ) {
                    final User user = dataSnapshot.getValue(User.class);
                    user.setTable_id(null);
                    user.setEstablishment_id(null);
                    dataSnapshot.getRef().setValue(user);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Interfaces.OnFragmentInteractionListener && context instanceof Interfaces.OnLoaderAction) {
            mListener = (Interfaces.OnFragmentInteractionListener) context;
            mListenerLoader = (Interfaces.OnLoaderAction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private View getDialog(){
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View dialogView = factory.inflate(R.layout.dialog, null);

        final TextView title = dialogView.findViewById(R.id.title_dialog);
        title.setText(getActivity().getText(R.string.dialog_attention));
        final TextView message = dialogView.findViewById(R.id.message_dialog);
        message.setText(getActivity().getText(R.string.dialog_attention_content));
        final ImageView image = dialogView.findViewById(R.id.image_dialog);
        image.setImageResource(R.drawable.warning);

        return dialogView;
    }

}
