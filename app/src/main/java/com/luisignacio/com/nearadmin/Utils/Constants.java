package com.luisignacio.com.nearadmin.Utils;

/**
 * Created by Nacho on 14/04/2018.
 */

public final class Constants {

    public static final String USERS = "users";
    public static final String WAITERS = "waiters";
    public static final String TOKEN_CM = "token_cm";
    public static final String ESTABLISHMENTS = "establishment";
    public static final String TABLES = "tables";
    public static final int STATE_ORDER = 1, STATE_ORDER_BILL = 3, STATE_ATTENTION = 2, STATE_NORMAL = 0;
    public static final String NAME = "name",
                               EMAIL = "email",
                               PHOTO = "photo",
                               ESTABLISHMENT_ID = "establishment",
                               TABLE_ID = "table";
    public static final int ERROR_TABLE_NOT_FOUND = 0, ERROR_OCCUPIED_TABLE = 1;

}
