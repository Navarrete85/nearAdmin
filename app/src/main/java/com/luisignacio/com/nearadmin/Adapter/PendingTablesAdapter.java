package com.luisignacio.com.nearadmin.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Utils.CustomTimer;
import com.luisignacio.com.nearadmin.Utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import android.os.Handler;

public class PendingTablesAdapter extends RecyclerView.Adapter<PendingTablesAdapter.MyViewHolder>{

    private static final String TAG = PendingTablesAdapter.class.getSimpleName();

    private Context ctx;
    private HashMap<String, Table> mTables;
    private View mParent;
    private Interfaces.RecyclerViewCLickListener mListener;
    private Interfaces.OnListEmpty mEmptyListener;
    private ArrayList<Table> mTablesList;
    private Handler handler = new Handler();

    public PendingTablesAdapter(Context ctx, HashMap<String, Table> mTables, View mParent, Interfaces.RecyclerViewCLickListener mListener, Interfaces.OnListEmpty emptyListener) {
        this.ctx = ctx;

        this.mTables = mTables;
        this.mParent = mParent;
        this.mListener = mListener;
        this.mEmptyListener = emptyListener;
        init();
    }

    public void clearAll() {
        handler.removeCallbacksAndMessages(null);
    }

    public void setTablesState(HashMap<String, Table> tables ){
        mTables = tables;
        init();
        notifyDataSetChanged();
    }

    private void init() {
        mTablesList = new ArrayList<>();

        for ( Table item : mTables.values() ){
            if ( item.getState() > 0 )
                mTablesList.add(item);
        }

        Collections.sort(mTablesList, new Comparator<Table>() {
            @Override
            public int compare(Table table1, Table table2) {
                if ( table1.getTime_of_state_change() == null || table2.getTime_of_state_change() == null)
                    return 0;
                else{
                    return Utils.getTime(table1.getTime_of_state_change()).compareTo(Utils.getTime(table2.getTime_of_state_change()));
                }
            }
        });


        Collections.reverse(mTablesList);
        mEmptyListener.isEmpty((mTablesList.size()==0)?true:false);
    }

    @Override
    public PendingTablesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(ctx).inflate(R.layout.table_state_item, parent, false);
        final PendingTablesAdapter.MyViewHolder viewHolder = new PendingTablesAdapter.MyViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PendingTablesAdapter.MyViewHolder holder, int position) {
        holder.binItem(mTablesList.get(position));
    }

    @Override
    public int getItemCount() {
        return mTablesList.size();
    }

    public Table getItem(int index){
        return ((mTablesList != null && mTablesList.size() > index)?mTablesList.get(index):null);
    }

    protected  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mTableId, mTvOccupiedContent, mTvWaitingTime;
        private Interfaces.RecyclerViewCLickListener mListener;
        private CustomTimer customTimer;

        public MyViewHolder(View itemView, Interfaces.RecyclerViewCLickListener listener) {
            super(itemView);

            mListener = listener;
            mTableId = itemView.findViewById(R.id.table_id);
            mTvOccupiedContent = itemView.findViewById(R.id.tvOccupiedContent);
            mTvWaitingTime = itemView.findViewById(R.id.waiting_time);
            customTimer = new CustomTimer(handler, mTvWaitingTime, 10000);

        }

        public void binItem( Table table ){
            mTableId.setText(table.getId());
            mTableId.getBackground().setLevel(table.getState());
            mTvOccupiedContent.setText(Utils.getStringTime(table.getStart_time()));
            handler.removeCallbacks(customTimer);
            customTimer.holder = mTvWaitingTime;
            customTimer.millisUntilFinished = Utils.getTimeInMillisBetweenDate(table.getTime_of_state_change());
            handler.postDelayed(customTimer, 100);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }

    }

}
