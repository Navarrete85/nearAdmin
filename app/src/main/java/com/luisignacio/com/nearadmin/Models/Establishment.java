package com.luisignacio.com.nearadmin.Models;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Establishment implements Serializable, Parcelable {

    private String  uuid;
    private ArrayList<Double> location;
    private ArrayList<String> photoUrl;
    private ArrayList<String> offers;
    private float ocupation;
    private String  description;
    private ArrayList<String> workers_id;
    private String name;
    private String image_logo;
    private float distance;
    private String waiter_id;

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setOcupation(float ocupation) {
        this.ocupation = ocupation;
    }

    public float getDistance() {
        return distance;
    }

    public String getImage_logo() {return image_logo;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Establishment() {
    }
    public String getUuid() {
        return uuid;
    }

    public ArrayList<Double> getLocation() {
        return location;
    }

    public ArrayList<String> getPhotoUrl() {
        return photoUrl;
    }

    public ArrayList<String> getOffers() {
        return offers;
    }

    public String getDescription() {
        return description;
    }

    public float getOcupation() {return ocupation;}

    public ArrayList<String> getWorkers_id() {return workers_id;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uuid);
        dest.writeList(this.location);
        dest.writeStringList(this.photoUrl);
        dest.writeStringList(this.offers);
        dest.writeFloat(this.ocupation);
        dest.writeString(this.description);
        dest.writeStringList(this.workers_id);
        dest.writeString(this.name);
        dest.writeString(this.image_logo);
        dest.writeFloat(this.distance);
        dest.writeString(this.waiter_id);
    }

    protected Establishment(Parcel in) {
        this.uuid = in.readString();
        this.location = new ArrayList<Double>();
        in.readList(this.location, Double.class.getClassLoader());
        this.photoUrl = in.createStringArrayList();
        this.offers = in.createStringArrayList();
        this.ocupation = in.readFloat();
        this.description = in.readString();
        this.workers_id = in.createStringArrayList();
        this.name = in.readString();
        this.image_logo = in.readString();
        this.distance = in.readFloat();
        this.waiter_id = in.readString();
    }

    public static final Creator<Establishment> CREATOR = new Creator<Establishment>() {
        @Override
        public Establishment createFromParcel(Parcel source) {
            return new Establishment(source);
        }

        @Override
        public Establishment[] newArray(int size) {
            return new Establishment[size];
        }
    };
}
