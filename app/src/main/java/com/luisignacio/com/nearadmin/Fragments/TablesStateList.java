package com.luisignacio.com.nearadmin.Fragments;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luisignacio.com.nearadmin.Activitys.MainActivity;
import com.luisignacio.com.nearadmin.Adapter.PendingTablesAdapter;
import com.luisignacio.com.nearadmin.Adapter.SwipeController;
import com.luisignacio.com.nearadmin.Adapter.SwipeControllerActions;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.Models.Tables;
import com.luisignacio.com.nearadmin.Models.User;
import com.luisignacio.com.nearadmin.R;
import static com.luisignacio.com.nearadmin.Utils.Constants.*;

import java.util.HashMap;


public class TablesStateList extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = TablesStateList.class.getSimpleName();

    private Tables mTables;
    private String mParam2;

    private View mView;
    private RecyclerView mRecyclerView;
    private PendingTablesAdapter mAdapter;
    private View mEmptyView;


    private Interfaces.OnFragmentInteractionListener mListener;
    private Interfaces.OnLoaderAction mListenerLoader;

    public TablesStateList() {}

    public void setmTables(Tables mTables) {
        this.mTables = mTables;
        if ( mAdapter != null )
            mAdapter.setTablesState(mTables.getTables());
        else{
//            android.support.v4.app.FragmentTransaction ft = this.getFragmentManager().beginTransaction();
//            ft.detach(this).attach(this).commit();
        }


    }

    public static TablesStateList newInstance(Tables tables, String param2) {
        TablesStateList fragment = new TablesStateList();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, tables);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTables = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_tables_state_list, container, false);
        mRecyclerView = mView.findViewById(R.id.tables_list);
        mEmptyView = mView.findViewById(R.id.empty_list);
        Interfaces.RecyclerViewCLickListener listener = (view, position) -> {
            Snackbar.make(getView(),mAdapter.getItem(position).getId() + " --> " + mAdapter.getItem(position).getState(), Toast.LENGTH_SHORT).show();
        };

        Interfaces.OnListEmpty emptyListener = ((isEmpty) -> {
            if ((isEmpty)) {
                mEmptyView.setVisibility(View.VISIBLE);
            } else {
                mEmptyView.setVisibility(View.GONE);
            }
        });

        mAdapter = new PendingTablesAdapter(getContext(), (mTables == null || mTables.getTables() == null)?new HashMap<>():mTables.getTables(), mView, listener, emptyListener);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        SwipeController swipeController = new SwipeController(new SwipeControllerActions(){
            @Override
            public void onRightClicked(int position) {
                final Table item = mAdapter.getItem(position);
                showDialog(item);
                super.onRightClicked(position);
            }
        }, getContext());
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });


        return mView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Interfaces.OnFragmentInteractionListener && context instanceof Interfaces.OnLoaderAction) {
            mListener = (Interfaces.OnFragmentInteractionListener) context;
            mListenerLoader = (Interfaces.OnLoaderAction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void showDialog( Table item ){
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View dialogView = factory.inflate(R.layout.dialog, null);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        final TextView title = dialogView.findViewById(R.id.title_dialog);
        title.setText(getActivity().getText(R.string.dialog_attention));
        final TextView message = dialogView.findViewById(R.id.message_dialog);
        message.setText(getActivity().getText(R.string.dialog_attention_content));
        final ImageView image = dialogView.findViewById(R.id.image_dialog);
        image.setImageResource(R.drawable.warning);
        dialog.setView(dialogView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogView.findViewById(R.id.btn_dialog).setOnClickListener((view) -> {
            mListenerLoader.onStarLoader(view);
            final int last_state = item.getState();
            item.setTime_of_state_change(null);
            item.setState(0);
            final String establishment_id = ((MainActivity)getActivity()).getmEstablishment_id();
            FirebaseDatabase.getInstance().getReference().getRoot().child(TABLES).child(establishment_id).child(item.getTable_id()).setValue(item).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.i(TAG, (task.isComplete() ? "Completado" : "No completado"));
                    Log.i(TAG, task.isSuccessful() + " Exito?");
                    mListenerLoader.onStopLoader(view);
                    if ( last_state == 3 ){
                        endConnection(item);
                    }
                }
            });
            dialog.dismiss();
        });
        final Button cancel = dialogView.findViewById(R.id.btn_dialog_cancel);
        cancel.setVisibility(View.VISIBLE);
        cancel.setOnClickListener((view) -> dialog.dismiss());
        dialog.show();
    }

    private void endConnection(Table item) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View dialogView = factory.inflate(R.layout.dialog, null);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        final TextView title = dialogView.findViewById(R.id.title_dialog);
        title.setText(getActivity().getText(R.string.dialog_disconnect));
        final TextView message = dialogView.findViewById(R.id.message_dialog);
        message.setText(getActivity().getText(R.string.dialog_disconnect_content));
        final ImageView image = dialogView.findViewById(R.id.image_dialog);
        image.setImageResource(R.drawable.disconnect);
        dialog.setView(dialogView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final Button disconnect = dialogView.findViewById(R.id.btn_dialog_cancel);
        disconnect.setVisibility(View.VISIBLE);
        disconnect.setText(getActivity().getString(R.string.disconnect));

        disconnect.setOnClickListener((view) -> {
            final String establishment_id = ((MainActivity)getActivity()).getmEstablishment_id();
            FirebaseDatabase.getInstance().getReference().getRoot().child(USERS).child(item.getUser_id()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        final User user = dataSnapshot.getValue(User.class);
                        user.setEstablishment_id(null);
                        user.setTable_id(null);
                        dataSnapshot.getRef().setValue(user);
                    }
                    mListenerLoader.onStopLoader(null);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            item.setUser_id(null);
            item.setTime_of_state_change(null);
            item.setStart_time(null);
            FirebaseDatabase.getInstance().getReference().getRoot().child(TABLES).child(establishment_id).child(item.getTable_id()).setValue(item);
            dialog.dismiss();
        });

        final Button cancel = dialogView.findViewById(R.id.btn_dialog);
        cancel.setVisibility(View.VISIBLE);
        cancel.setText(getActivity().getString(R.string.cancel));
        cancel.setOnClickListener((view2) -> dialog.dismiss());
        dialog.setCancelable(true);
        dialog.show();

    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.clearAll();
    }
}
