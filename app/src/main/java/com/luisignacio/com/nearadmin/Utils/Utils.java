package com.luisignacio.com.nearadmin.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.Models.Tables;
import com.luisignacio.com.nearadmin.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Pattern;
import static com.luisignacio.com.nearadmin.Utils.Constants.*;

/**
 * Created by Nacho on 08/04/2018.
 */

public class Utils {

    private static final String TAG = "Utils";

    public final static boolean isValidEmail(final CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPasswd(final CharSequence passwd) {

        final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*[A-Z]).{6,20})";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        return pattern.matcher(passwd).matches();

    }

    public static AlertDialog.Builder getAlertBuilder(int title, int msg, int icon, Context context){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(msg)
                .setTitle(title)
                .setIcon(icon);

        return builder;
    }

    public static void getAuthError(Exception exception, Context context, TextInputLayout mTxtEmail, TextInputLayout mTxtPasswd){

        try {
            throw exception;
        } catch(FirebaseAuthWeakPasswordException e) {
            mTxtPasswd.setError(context.getString(R.string.passwd_error));
            mTxtPasswd.requestFocus();
        } catch(FirebaseAuthInvalidCredentialsException e) {
            mTxtEmail.setError(context.getString(R.string.email_error));
            mTxtEmail.requestFocus();
        } catch(FirebaseAuthUserCollisionException e) {
            mTxtEmail.setError(context.getString(R.string.error_user_exists));
            mTxtEmail.requestFocus();
        }catch (FirebaseAuthInvalidUserException e){
            mTxtEmail.setError(context.getString(R.string.invalid_email));
            mTxtEmail.requestFocus();
        }
        catch(Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public static Bitmap getBitmapFromURL(String src) {

        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public static DatabaseReference getUserReference(String user_uid, String child ){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child(Constants.USERS).child(user_uid).child(child);

        return reference;
    }

    public static boolean isBetween(float number, float lower, float upper) {
        return lower <= number && number <= upper;
    }

    public static String getStringCurrentTime(){
        final DateFormat dateFormat = getFormat(TimeFormats.Normal);
        final Date date = new Date();
        return dateFormat.format(date).toString();
    }

    public static String getStringTime(String time){
        final DateFormat dateFormat = getFormat(TimeFormats.Normal);
        try{
            final Date date = dateFormat.parse(time);
            return getFormat(TimeFormats.Show).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }



    private static DateFormat getFormat(TimeFormats type){
        return ((type == TimeFormats.Normal) ? new SimpleDateFormat("yyyy/MM/dd HH:mm:ss") : new SimpleDateFormat("HH:mm"));
    }

    public static long getTimeInMillisBetweenDate( String dateSince ){
        long result = 0L;
        final DateFormat format = getFormat(TimeFormats.Normal);
        Date date;
        try {
            if ( dateSince == null )
                date = format.parse(getStringCurrentTime());
            else
                date = format.parse(dateSince);
            Date now = new Date();
            result = now.getTime() - date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String getTime(String date){
        long seconds = getTimeInMillisBetweenDate(date) / 1000;
        long minutes = seconds / 60;

        return minutes % 60 + ":" + seconds % 60;

    }

    public static int giveMeOcuppation(Tables tables){

        int count = 0;
        for ( Table item : tables.getTables().values() ){
            if ( item.getUser_id() != null )
                count++;
        }
        return (count * 100) / tables.getTables().size();
    }

    public static void checkState(Tables tables, String establishment_id){
        new Thread(() -> {
            for ( Map.Entry<String, Table> item : tables.getTables().entrySet() ){
                if ( item.getValue().getState() != 0 && item.getValue().getTime_of_state_change() == null ){
                   item.getValue().setTime_of_state_change(getStringCurrentTime());
                   FirebaseDatabase.getInstance().getReference().getRoot().child(TABLES).child(establishment_id).child(item.getKey()).setValue(item.getValue());
               }
            }
        }).start();
    }

    enum TimeFormats{
        Normal,
        Show
    }

}
