package com.luisignacio.com.nearadmin.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luisignacio.com.nearadmin.Activitys.MainActivity;
import com.luisignacio.com.nearadmin.Adapter.ViewPagerAdapter;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Tables;
import com.luisignacio.com.nearadmin.R;
import java.util.HashMap;
import java.util.Map;

public class MainFragment extends Fragment implements Interfaces.OnTableChangeListener {

    private static final String TAG = "MainFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String FRAGMENT_TABLES = "Todas",
                                FRAGMENT_TABLES_ESTATE = "Pendientes";

    private String[] keys = {FRAGMENT_TABLES, FRAGMENT_TABLES_ESTATE};
    private String mParam1;
    private String mParam2;
    private View mView;
    private Map<String, Fragment> mFragments;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private Tables mTables;

    private Interfaces.OnFragmentInteractionListener mListener;

    public MainFragment() {}

    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mFragments = new HashMap<>();
        if (mTables == null )
            mTables = ((MainActivity)getActivity()).getmTables();
        mFragments.put(FRAGMENT_TABLES, TablesList.newInstance(mTables, ""));
        mFragments.put(FRAGMENT_TABLES_ESTATE, TablesStateList.newInstance(mTables, ""));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_main, container, false);

        mTabLayout = mView.findViewById(R.id.tab_layout_id);
        mViewPager = mView.findViewById(R.id.view_pager_id);
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        for (String key : keys)
            mViewPagerAdapter.addFragment(mFragments.get(key), key);

        mViewPager.setAdapter(mViewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        ((MainActivity)getActivity()).setOnTableChangeListener(this);

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Interfaces.OnFragmentInteractionListener) {
            mListener = (Interfaces.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnTableStateChange(Tables tables) {
        mTables = tables;
        ((TablesList)mFragments.get(FRAGMENT_TABLES)).setmTables(mTables);
        ((TablesStateList)mFragments.get(FRAGMENT_TABLES_ESTATE)).setmTables(mTables);
    }
}
