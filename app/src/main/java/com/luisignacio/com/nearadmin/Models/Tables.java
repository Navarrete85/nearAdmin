package com.luisignacio.com.nearadmin.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Nacho on 14/04/2018.
 */

public class Tables implements Parcelable {

   private HashMap<String, Table> tables;

    public Tables() {
        tables = new HashMap<>();
    }

    public HashMap<String, Table> getTables() {
        return tables;
    }

    public void setTables(HashMap<String, Table> tables) {
        this.tables = tables;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.tables);
    }

    protected Tables(Parcel in) {
        this.tables = (HashMap<String, Table>) in.readSerializable();
    }

    public static final Parcelable.Creator<Tables> CREATOR = new Parcelable.Creator<Tables>() {
        @Override
        public Tables createFromParcel(Parcel source) {
            return new Tables(source);
        }

        @Override
        public Tables[] newArray(int size) {
            return new Tables[size];
        }
    };
}
