package com.luisignacio.com.nearadmin.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luisignacio.com.nearadmin.Activitys.MainActivity;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces.*;

import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.luisignacio.com.nearadmin.Models.Waiters;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Utils.Utils;
import static com.luisignacio.com.nearadmin.Utils.Constants.*;
import static com.luisignacio.com.nearadmin.Interfaces.Interfaces.OnFragmentLoginErrorListener.*;

/**
 * Fragmento para realizar nuestro login de usuario administrador
 */
public class LoginFragment extends android.support.v4.app.Fragment implements FirebaseAuth.AuthStateListener {

    private static final String TAG = "LoginFragment";
    private OnFragmentLoginErrorListener mListener;
    private OnLoaderAction mLoader;
    private View mView;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private Button mLoginButton;
    private TextInputLayout mTxtEmail, mTxtPasswd;
    private FirebaseUser mFirebaseUser;
    private Context mCtx;

    public LoginFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        mFirebaseUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mView = inflater.inflate(R.layout.fragment_login, container, false);

        initComponent();

        return mView;
    }

    private void initComponent() {
        mLoginButton = mView.findViewById(R.id.btn_login);
        mTxtEmail= mView.findViewById(R.id.txtLayoutEmail);
        mTxtPasswd = mView.findViewById(R.id.txtLayoutPasswd);
        mLoginButton.setOnClickListener((view -> checkLogin()));
    }

    private void checkLogin() {
        final CharSequence email = mTxtEmail.getEditText().getText(),
                passwd = mTxtPasswd.getEditText().getText();

        if ( validate( email, passwd ) ){

            if ( mFirebaseUser == null ){
                mLoader.onStarLoader(mView);
                mAuth.signInWithEmailAndPassword(email.toString().trim(), passwd.toString().trim())
                        .addOnCompleteListener(getActivity(), task ->{
                            mLoader.onStopLoader(mView);
                            if ( !task.isSuccessful() )
                                mListener.onShowError(ERROR_LOGIN);
                        });
            }else{
                mAuth.signOut();
            }

        }

    }

    /**
     * Método con el que el contenido introducido en los campos de email y passwd son correctos, en caso de que
     * algún campo no siga con el patrón establecido mostramos el error en el respectivo campo de insercción de datos
     * @param email
     * @param passwd
     * @return
     */
    private boolean validate( CharSequence email, CharSequence passwd ){

        mTxtPasswd.setError(null);
        mTxtEmail.setError(null);
        mTxtEmail.setErrorEnabled(false);
        mTxtPasswd.setErrorEnabled(false);

        if ( !Utils.isValidEmail(email) ){

            mTxtEmail.setErrorEnabled(true);

            if ( email.toString().trim().isEmpty() )
                mTxtEmail.setError(getString(R.string.empty_email));
            else
                mTxtEmail.setError(getString(R.string.email_error));

            return false;

        }

        if ( !Utils.isValidPasswd(passwd) ){

            mTxtPasswd.setErrorEnabled(true);

            if ( passwd.toString().trim().isEmpty() )
                mTxtPasswd.setError(getString(R.string.empty_passwd));
            else
                mTxtPasswd.setError(getString(R.string.passwd_error));

            return false;

        }

        return true;

    }

    /**
     * Nos recogemos la instancia para poner a la escucha el listener
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentLoginErrorListener) {
            mListener = (OnFragmentLoginErrorListener) context;
            mLoader = (OnLoaderAction) context;
            mCtx = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Destruimos el listener
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mCtx = null;
    }

    /**
     * Listener para el cambio de estado del usuario
     * @param firebaseAuth
     */
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if ( currentUser != null){
            mLoader.onStarLoader(mView);
            final DatabaseReference reference = FirebaseDatabase.getInstance().getReference().getRoot().child(WAITERS).child(currentUser.getUid());
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mLoader.onStopLoader(mView);
                    if (dataSnapshot.exists()){
                        final Waiters waiter = dataSnapshot.getValue(Waiters.class);
                        if ( waiter.isIs_activate() ){
                            goToMain(waiter.getEstablishment_id());
                        }else{
                            mListener.onShowError(ERROR_INACTIVATE);
                        }
                    }else{
                        mListener.onShowError(ERROR_PERMISSION);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void goToMain(String establishment_id) {

        Intent intent;

        if (isAdded()){
            intent = new Intent(getContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ESTABLISHMENT_ID, establishment_id);
            startActivity(intent);
        }

    }

}
