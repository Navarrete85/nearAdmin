package com.luisignacio.com.nearadmin.Activitys;

import android.app.AlertDialog;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import android.support.v4.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.luisignacio.com.nearadmin.Fragments.LoginFragment;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces.*;

public class AccesActivity extends AppCompatActivity implements OnLoaderAction, OnFragmentLoginErrorListener {

    private static final String TAG = "AccessActivity";
    private VideoView mVideoView;
    private ImageView mLogo;
    private LoginFragment mLoginFragment;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acces);

        mVideoView = findViewById(R.id.videoView);
        Uri path = Uri.parse("android.resource://com.luisignacio.com.nearadmin/" + R.raw.background2);

        mLogo = findViewById(R.id.logo);
        mProgressBar = findViewById(R.id.accessProgressBar);

        mVideoView.setVideoURI(path);
        mVideoView.setOnPreparedListener((mediaPlayer) ->{
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        });

        mLoginFragment = new LoginFragment();
        changeFragment(mLoginFragment, false);
    }

    /**
     * Cuando llamamos a onStart, comenzamos la nimación del logo jugando con la capa alfa
     */
    @Override
    protected void onStart() {
        super.onStart();
        Animation animation = new AlphaAnimation(1, 0.5f);
        animation.setDuration(6000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        mLogo.startAnimation(animation);
    }

    /**
     * Método que se llama cuando queremos cambiar de fragmento a presentar
     * @param fragment --> Fragmento a presentar
     * @param backStack --> Boolean que indica si queremos mantener el fragmento a reemplazar en la pila
     */
    private void changeFragment(Fragment fragment, boolean backStack ){

        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);

        if ( !backStack ) {
            ft.replace(R.id.contentLayout, fragment).commit();
        }else {
            ft.replace(R.id.contentLayout, fragment).addToBackStack("tag").commit();
        }
    }



    @Override
    public void onStarLoader(View view) {
        view.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopLoader(View view) {
        view.setEnabled(true);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onShowError(int error) {
        int imageResources, titleResources, messageResources;
        imageResources = R.drawable.permission_denied;

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if ( mAuth.getCurrentUser() != null )
            mAuth.signOut();

        switch (error){
            case ERROR_LOGIN:
                imageResources = R.drawable.login_error;
                titleResources = R.string.error_user_passwd_title;
                messageResources = R.string.error_user_passwd_message;
                break;
            case ERROR_INACTIVATE:
                titleResources = R.string.error_user_deactivate_title;
                messageResources = R.string.error_user_deactivate_message;
                break;
            default:
                titleResources = R.string.error_permission_title;
                messageResources = R.string.error_permission_message;
                break;
        }
        LayoutInflater factory = LayoutInflater.from(this);
        final View dialogView = factory.inflate(R.layout.dialog, null);
        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        final TextView title = dialogView.findViewById(R.id.title_dialog);
        title.setText(getText(titleResources));
        final TextView message = dialogView.findViewById(R.id.message_dialog);
        message.setText(getText(messageResources));
        final ImageView image = dialogView.findViewById(R.id.image_dialog);
        image.setImageResource(imageResources);
        dialog.setView(dialogView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogView.findViewById(R.id.btn_dialog).setOnClickListener((view) -> dialog.dismiss());

        dialog.setCancelable(false);
        dialog.show();
    }
}
