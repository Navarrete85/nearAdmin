package com.luisignacio.com.nearadmin.Activitys;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.luisignacio.com.nearadmin.Fragments.MainFragment;
import com.luisignacio.com.nearadmin.Interfaces.Interfaces;
import com.luisignacio.com.nearadmin.Models.Establishment;
import com.luisignacio.com.nearadmin.Models.Table;
import com.luisignacio.com.nearadmin.Models.Tables;
import com.luisignacio.com.nearadmin.R;
import com.luisignacio.com.nearadmin.Utils.Utils;

import static com.luisignacio.com.nearadmin.Utils.Constants.*;

public class MainActivity extends AppCompatActivity implements Interfaces.OnFragmentInteractionListener, FirebaseAuth.AuthStateListener, Interfaces.OnLoaderAction {

    private static final String TAG = "MainActivity";

    private FirebaseAuth mAuth;
    private DatabaseReference mTablesReference;
    private ValueEventListener mTablesListener;
    private Context mCtx;
    private Interfaces.OnTableChangeListener mTableEstateListener;
    private String mEstablishment_id;
    private View mLoader;
    private Tables mTables;
    private boolean mStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setElevation(0);

        mEstablishment_id = getIntent().getExtras().getString(ESTABLISHMENT_ID);
        mCtx = this;
        mLoader = findViewById(R.id.loader_main);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        ft.replace(R.id.content_fragment, MainFragment.newInstance("","")).commit();

        mAuth = FirebaseAuth.getInstance();
        mAuth.addAuthStateListener(this);
        initFCM();
    }

    public String getmEstablishment_id() {
        return mEstablishment_id;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mStart = true;
        onStarLoader(null);
        mTablesReference = FirebaseDatabase.getInstance().getReference().getRoot().child(TABLES).child(mEstablishment_id);
        mTablesListener = mTablesReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("Entro en onstart");
                if ( dataSnapshot.exists() ){
                    if ( mStart ) {
                        onStopLoader(null);
                        mStart = false;
                    }
                    final Tables tables = new Tables();

                    for ( DataSnapshot child : dataSnapshot.getChildren() ){
                        final Table table = child.getValue(Table.class);
                        tables.getTables().put(child.getKey(), table);
                    }

                    updateOcupation(tables);
                    if ( mTableEstateListener != null )
                        mTableEstateListener.OnTableStateChange(tables);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateOcupation(Tables tables) {
        FirebaseDatabase.getInstance().getReference().getRoot().child(ESTABLISHMENTS).child(mEstablishment_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if ( dataSnapshot.exists() ){
                    Utils.checkState(tables, mEstablishment_id);
                    final Establishment establishment = dataSnapshot.getValue(Establishment.class);
                    establishment.setOcupation(Utils.giveMeOcuppation(tables));
                    dataSnapshot.getRef().setValue(establishment);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStop() {
        if ( mTablesReference != null && mTablesListener != null ){
            mTablesReference.removeEventListener(mTablesListener);
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater(); inflater.inflate(R.menu.menu, menu);
        return super .onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.close_session ){
            mAuth.signOut();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setOnTableChangeListener(Interfaces.OnTableChangeListener listener){
        mTableEstateListener = listener;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        Log.i(TAG, "cambio estado usuario --> " + firebaseAuth.toString());
        if ( firebaseAuth.getCurrentUser() != null)
            Log.i(TAG, "cambio estado usuario --> " + firebaseAuth.getCurrentUser().getUid());

        if ( firebaseAuth.getCurrentUser() == null ){

            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
    }


    public Tables getmTables() {
        return mTables;
    }

    @Override
    public void onStarLoader(View view) {
        mLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopLoader(View view) {
            mLoader.setVisibility(View.GONE);
    }

    /**
     * Función encargada de guardar nuestro token de cloud messaging en nuestra base de datos (El token para las notificaciones
     * sólamente lo va a tener la app nearAdmin)
     * @param token
     */
    private void sendRegistrationToServer(String token) {
        Log.d(TAG, "sendRegistrationToServer: sending token to server: " + token);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child(WAITERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(TOKEN_CM)
                .setValue(token);
    }

    /**
     * Iniciamos firebase cloud messaging
     */
    private void initFCM(){
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "initFCM: token: " + token);
        sendRegistrationToServer(token);
    }

}
