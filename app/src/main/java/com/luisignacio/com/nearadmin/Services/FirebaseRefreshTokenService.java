package com.luisignacio.com.nearadmin.Services;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import static com.luisignacio.com.nearadmin.Utils.Constants.*;

/**
 * Servicio firebase que se va a encargar de actualizar el token en nuestra base de datos, para que cuando este
 * cambie lo actualicemos y podamos enviar notificaciones al usuario
 */
public class FirebaseRefreshTokenService extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseRefreshTokenService.class.getSimpleName();

    /**
     * Método que se llama cuando se actualiza el token de firebase
     */
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        final String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "Nuevo token --> " + refreshToken);
        sendRegistrationToServer(refreshToken);

    }

    /**
     * Método que utilizamos para guardar nuestro nuevo token en la base de datos
     * @param token
     */
    private void sendRegistrationToServer(String token) {
        if ( FirebaseAuth.getInstance().getCurrentUser() != null ) {
            Log.d(TAG, "sendRegistrationToServer: sending token to server: " + token);
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference().getRoot();
            reference.child(WAITERS)
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(TOKEN_CM)
                    .setValue(token);
        }
    }

}
