package com.luisignacio.com.nearadmin.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luisignacio.com.nearadmin.Models.Waiters;
import com.luisignacio.com.nearadmin.R;

import static com.luisignacio.com.nearadmin.Utils.Constants.*;

public class SplashActivity extends AppCompatActivity {

    public static final String TAG = "SplashActivity";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mAuth = FirebaseAuth.getInstance();

        if ( mAuth.getCurrentUser() != null ){
            checkIfUserIsActivate();
        }else{
            goTo(new Intent(SplashActivity.this, AccesActivity.class), null);
        }

    }

    private void goTo(Intent intent, String establishment_id) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        if ( establishment_id != null )
            intent.putExtra(ESTABLISHMENT_ID, establishment_id);
        startActivity(intent);
    }

    private void checkIfUserIsActivate() {
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference().getRoot().child(WAITERS).child(mAuth.getCurrentUser().getUid());
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    final Waiters waiter = dataSnapshot.getValue(Waiters.class);
                    if ( waiter.isIs_activate() ){
                        goTo(new Intent(SplashActivity.this, MainActivity.class), waiter.getEstablishment_id());
                        return;
                    }else{
                        mAuth.signOut();
                    }
                }
                goTo(new Intent(SplashActivity.this, AccesActivity.class), null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
