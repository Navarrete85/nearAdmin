package com.luisignacio.com.nearadmin.Interfaces;

import android.net.Uri;
import android.view.View;

import com.luisignacio.com.nearadmin.Models.Tables;

public class Interfaces {
    /**
     * Interfar para comunicar el fragmento login con la actividad Access
     */
    public interface OnFragmentLoginErrorListener {
        int ERROR_PERMISSION = 0, ERROR_INACTIVATE = 1, ERROR_LOGIN = 2;
        void onShowError(int error);
    }

    /**
     * Interfaz para activar o desactivar desde un fragmento un loader de una actividad
     */
    public interface OnLoaderAction{
        void onStarLoader(View view);
        void onStopLoader(View view);
    }

    public interface  OnTableChangeListener{
        void OnTableStateChange(Tables tables);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public interface RecyclerViewCLickListener {
        void onClick(View view, int position);
    }

    public interface OnListEmpty{
        void isEmpty(boolean isEmpty);
    }

}
