package com.luisignacio.com.nearadmin.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Waiters implements Parcelable {

    private String establishment_id;
    private boolean is_activate;
    private String token_cm;

    public String getToken_cm() {
        return token_cm;
    }

    public Waiters() {
    }

    public String getEstablishment_id() {
        return establishment_id;
    }

    public void setEstablishment_id(String establishment_id) {
        this.establishment_id = establishment_id;
    }

    public boolean isIs_activate() {
        return is_activate;
    }

    public void setIs_activate(boolean is_activate) {
        this.is_activate = is_activate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.establishment_id);
        dest.writeByte(this.is_activate ? (byte) 1 : (byte) 0);
        dest.writeString(this.token_cm);
    }

    protected Waiters(Parcel in) {
        this.establishment_id = in.readString();
        this.is_activate = in.readByte() != 0;
        this.token_cm = in.readString();
    }

    public static final Creator<Waiters> CREATOR = new Creator<Waiters>() {
        @Override
        public Waiters createFromParcel(Parcel source) {
            return new Waiters(source);
        }

        @Override
        public Waiters[] newArray(int size) {
            return new Waiters[size];
        }
    };
}
