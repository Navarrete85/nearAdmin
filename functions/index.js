let functions = require('firebase-functions');
let admin =  require ('firebase-admin');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp();

exports.sendNotification = functions.database.ref('/tables/{establishmentId}/{tableId}').onWrite((change,context) => {
	
	//Recogemos tanto el id del establecimiento como el de la mesa.
	
	console.log("parametros --> " + context.params.establishmentId);
	console.log("parametros --> " + context.params.tableId);
	const tableId = context.params.tableId;
	const establishmentId = context.params.establishmentId;

	//Cogemos el nombre de la mesa que ha sido afectada y el en el que está
	const tableName = change.after.child('id').val();
	const state =  change.after.child('state').val();
	
	

	//Log para mostrar la traza de los datos recogidos
	console.log("id_establecimiento: ", establishmentId);
	console.log("id_mesa_afectada: ", tableId);
	console.log("estado de la mesa: ", state);
	console.log("nombre de la mesa afectada: ", state);
	
	//Mensaje a mostrar al usuario que va a recibir la notificación
	var menssage;

	if ( state !== 0 ){
		switch(state){
			case 1:
				message = "El usuario de la mesa " + tableName + " quiere realizar un pedido";
				break;
			case 2:
				message = "El usuario de la mesa " + tableName + " quiere recibir atención personalizada";
				break;
			default:
				message = "El usuario de la mesa " + tableName + " ha pedido la cuenta";
				break;
		}

		console.log("mensaje: ", message);

		//Consulta para saber el id del camarero y poder recoger el token para enviarle la notificación
		return admin.database().ref("/establishment/" + establishmentId).once('value').then(snap => {
			const waiterId = snap.child("waiter_id").val();
			console.log("id camarero: ", waiterId);
			
			//Consulta para obtener el token del camarero y poder enciar la notificaicón
			return admin.database().ref("/waiters/" + waiterId).once('value').then(snap => {
				const token = snap.child("token_cm").val();
				console.log("token: ", token);
				
				//Construimos el mensaje a enviar a nuestra app cliente...
				console.log("Construimos la notificación...");
				const payload = {
					data: {
						data_type: "direct_message",
						title: "Nueva petición de atención",
						message: message,
						message_id: tableId,
					}
				};
				
				return admin.messaging().sendToDevice(token, payload)
							.then(function(response) {
								return console.log("Successfully sent message:", response);
							  })
							  .catch(function(error) {
							  	return console.log("Error sending message:", error);
							  });
			});
		});

	}else{
		return console.log("La mesa no ha cambiado de estado...así que no hago nada...")
	}
	
	
});
